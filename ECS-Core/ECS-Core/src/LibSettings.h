/*
	Defines the settings for the library, such as DLL export settings
*/
#pragma once

#ifdef UNITY_DEMO_EXPORTS
#define LIB_API __declspec(dllexport)
#elif  UNITY_DEMO_IMPORTS
#define LIB_API __declspec(dllimport)
#else
#define LIB_API
#endif

// Override if in debugging mode
#ifdef _DEBUG
#define LIB_API
#endif
