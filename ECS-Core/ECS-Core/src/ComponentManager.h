#pragma once

#include "IComponent.h"
#include <intrin.h>
#pragma intrinsic(_BitScanReverse)

unsigned long MSB(uint32_t val) {
	unsigned long result = 0;
	_BitScanReverse(&result, val);
	return result;
}

constexpr uint32_t NBA = 1024 / sizeof(uint32_t);

template <typename T>
class ComponentManager {
public:
	static T* Get(uint32_t entityId) {
		return myComponents + entityId;
	}

	static uint32_t Create(T*& outPtr) {
		for (int ix = 0; ix < NBA; ix++) {
			if (!myAllocBits[ix]) {
				uint32_t id = MSB(myAllocBits[ix]);
				myAllocBits[ix] |= 0x1 << id;
				id += sizeof(uint32_t) * ix;

				new (myComponents + id) T();
				outPtr = myComponents + id;

				// TODO: setup
				return id;
			}
		}
		return 0;
	}

	static void Delete(uint32_t id) {
		if (myAllocBits[id / sizeof(uint32_t)] & 0x01 << (id % sizeof(uint32_t))) {
			myAllocBits[id / sizeof(uint32_t)] ^= 0x01 << (id % sizeof(uint32_t));
			myComponents[id].~T();
		}
	}

private:
	static T myComponents[1024];
	static uint32_t myAllocBits[NBA];
};

template <typename T>
T ComponentManager<T>::myComponents[1024];

template <typename T>
uint32_t ComponentManager<T>::myAllocBits[NBA];